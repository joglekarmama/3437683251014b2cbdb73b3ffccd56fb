import React from 'react';
import ScratchCode from './components/scratchsand'


const App = () => {
  return (
    < div className='app-container'>
     
     <ScratchCode />
    
    </div>
  );
};

export default App;
